## New ParaView Classroom Tutorials

ParaView Classroom Tutorials have been moved from https://www.paraview.org/Wiki/ParaView_Classroom_Tutorials
to https://docs.paraview.org/en/latest/ClassroomTutorials/index.html.
